#!/bin/sh

# Add APT repository
test ! -f /bin/add-apt-repository && apt-get install -y software-properties-common
add-apt-repository -y --no-update universe
add-apt-repository -y --no-update multiverse
add-apt-repository ppa:ubuntuweb/main -y --no-update
mv /etc/apt/sources.list.d/ubuntuweb-ubuntu-**-**.list /etc/apt/sources.list.d/ubuntuweb.list
apt-get update

# Install the GUI stuff
apt-get install -y gnome-session gnome-shell-extension-dash-to-panel uweb-plymouth
apt-get install -y adapta-gtk-theme papirus-icon-theme --no-install-recommends
apt-get install -y ubuntu-web-default-settings ubuntu-web-backgrounds

# Install the web app deps
apt-get install -y winst wadk wapprmgui xterm filerun pass curl snow-web

# Install the default web apps
curl -L -O 'https://gitlab.e.foundation/rs2009/eapps/-/raw/master/prebuilt-wapp/e-apps.wapp' &>/dev/null
wappinst ./e-apps.wapp && rm -f ./e-apps.wapp
curl -L -O https://raw.githubusercontent.com/Ubuntu-Web/ecloud/main/ecloud.wapp &>/dev/null
wappinst ./ecloud.wapp && rm -f ./ecloud.wapp
curl -L -o /etc/xdg/autostart/ecloud-firstrun.desktop https://raw.githubusercontent.com/Ubuntu-Web/ecloud/main/ecloud-firstrun.desktop &>/dev/null
update-desktop-database &>/dev/null

# Hide apps and gnome-control-center panels which are not required and are confusing for newbies
mkdir -p /etc/skel/.local /etc/skel/.local/share /etc/skel/.local/share/applications
cat <<'EOF' >/etc/skel/.local/share/applications/gnome-online-accounts-panel.desktop
[Desktop Entry]
Name=Online Accounts
Comment=Connect to your online accounts and decide what to use them for
Exec=gnome-control-center online-accounts
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=goa-panel
Terminal=false
Type=Application
NoDisplay=true
StartupNotify=true
Categories=GNOME;GTK;Settings;
OnlyShowIn=GNOME;Unity;
X-GNOME-Bugzilla-Bugzilla=GNOME
X-GNOME-Bugzilla-Product=gnome-control-center
X-GNOME-Bugzilla-Component=Online Accounts
# Translators: Search terms to find the Online Accounts panel.
# Do NOT translate or localize the semicolons!
# The list MUST also end with a semicolon!
# For ReadItLater and Pocket, see http://en.wikipedia.org/wiki/Pocket_(application)
Keywords=Google;Facebook;Twitter;Yahoo;Web;Online;Chat;Calendar;Mail;Contact;ownCloud;Kerberos;IMAP;SMTP;Pocket;ReadItLater;
X-Ubuntu-Gettext-Domain=gnome-control-center-2.0
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/gnome-diagnostics-panel.desktop
[Desktop Entry]
Name=Diagnostics
Comment=Report your problems
Exec=gnome-control-center diagnostics
# FIXME
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=system-help
Terminal=false
Type=Application
NoDisplay=true
StartupNotify=true
Categories=GNOME;GTK;Settings;
OnlyShowIn=GNOME;Unity;
X-GNOME-Bugzilla-Bugzilla=GNOME
X-GNOME-Bugzilla-Product=gnome-control-center
X-GNOME-Bugzilla-Component=privacy
X-GNOME-Bugzilla-Version=3.38.1
# Translators: Search terms to find the Privacy panel. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
Keywords=screen;lock;diagnostics;crash;private;recent;temporary;tmp;index;name;network;identity;privacy;
X-Ubuntu-Gettext-Domain=gnome-control-center-2.0
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/yad-icon-browser.desktop
[Desktop Entry]
Encoding=UTF-8
Name=Icon Browser
Comment=Inspect GTK Icon Theme
Categories=GTK;Development;
Exec=yad-icon-browser
Icon=yad
Terminal=false
Type=Application
StartupNotify=true
Hidden=true
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/debian-xterm.desktop
[Desktop Entry]
Name=XTerm
#GenericName=Terminal
Comment=standard terminal emulator for the X window system
Exec=xterm
Terminal=false
Type=Application
Hidden=true
#Encoding=UTF-8
Icon=mini.xterm
Categories=System;TerminalEmulator;
Keywords=shell;prompt;command;commandline;cmd;
X-Desktop-File-Install-Version=0.24
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/debian-uxterm.desktop
[Desktop Entry]
Name=UXTerm
#GenericName=Terminal
Comment=xterm wrapper for Unicode environments
Exec=uxterm
Terminal=false
Type=Application
Hidden=true
#Encoding=UTF-8
Icon=mini.xterm
Categories=System;TerminalEmulator;
Keywords=shell;prompt;command;commandline;cmd;
X-Desktop-File-Install-Version=0.24
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/org.gnome.Nautilus.desktop
[Desktop Entry]
Name=Local Files
Comment=Access and organize local files
# Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
Keywords=folder;manager;explore;disk;filesystem;nautilus;
Exec=nautilus --new-window %U
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=org.gnome.Nautilus
Terminal=false
Type=Application
DBusActivatable=true
StartupNotify=true
Categories=GNOME;GTK;Utility;Core;FileManager;
MimeType=inode/directory;application/x-7z-compressed;application/x-7z-compressed-tar;application/x-bzip;application/x-bzip-compressed-tar;application/x-compress;application/x-compressed-tar;application/x-cpio;application/x-gzip;application/x-lha;application/x-lzip;application/x-lzip-compressed-tar;application/x-lzma;application/x-lzma-compressed-tar;application/x-tar;application/x-tarz;application/x-xar;application/x-xz;application/x-xz-compressed-tar;application/zip;application/gzip;application/bzip2;
X-GNOME-UsesNotifications=true
Actions=new-window;
X-Unity-IconBackgroundColor=#af4853
X-Ubuntu-Gettext-Domain=nautilus

[Desktop Action new-window]
Name=New Window
Exec=nautilus --new-window
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/org.gnome.eog.desktop
[Desktop Entry]
Name=Image Viewer
Comment=Browse and rotate images
TryExec=eog
Exec=eog %U
# TRANSLATORS: Do NOT translate or transliterate this text!
# This is an icon file name
Icon=org.gnome.eog
StartupNotify=true
Terminal=false
Type=Application
NoDisplay=true
Categories=GNOME;GTK;Graphics;2DGraphics;RasterGraphics;Viewer;
X-GNOME-Bugzilla-Bugzilla=GNOME
X-GNOME-Bugzilla-Product=EOG
X-GNOME-Bugzilla-Component=general
X-GNOME-Bugzilla-Version=3.38.0
X-GNOME-DocPath=eog/eog.xml
MimeType=image/bmp;image/gif;image/jpeg;image/jpg;image/pjpeg;image/png;image/tiff;image/x-bmp;image/x-gray;image/x-icb;image/x-ico;image/x-png;image/x-portable-anymap;image/x-portable-bitmap;image/x-portable-graymap;image/x-portable-pixmap;image/x-xbitmap;image/x-xpixmap;image/x-pcx;image/svg+xml;image/svg+xml-compressed;image/vnd.wap.wbmp;image/x-icns;
# Extra keywords that can be used to search for eog in GNOME Shell and Unity
# TRANSLATORS: Search terms to find this application.
#              Do NOT translate or localize the semicolons!
#              The list MUST also end with a semicolon!
Keywords=Picture;Slideshow;Graphics;
X-Ubuntu-Gettext-Domain=eog
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/info.desktop
[Desktop Entry]
Name=TeXInfo
Comment=The viewer for TexInfo documents
Exec=info
Icon=dialog-information
Type=Application
NoDisplay=true
Categories=Utility;Documentation;ConsoleOnly;
Terminal=true
X-Desktop-File-Install-Version=0.24
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/info.desktop
[Desktop Entry]
Name=TeXInfo
Comment=The viewer for TexInfo documents
Exec=info
Icon=dialog-information
Type=Application
NoDisplay=true
Categories=Utility;Documentation;ConsoleOnly;
Terminal=true
X-Desktop-File-Install-Version=0.24
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/org.gnome.gedit.desktop
[Desktop Entry]
Name=Text Editor
Comment=Edit text files
Exec=gedit %U
Terminal=false
Type=Application
NoDisplay=true
StartupNotify=true
MimeType=text/plain;
# TRANSLATORS: Do NOT translate or transliterate this text!
#              This is an icon file name.
Icon=org.gnome.gedit
Categories=GNOME;GTK;Utility;TextEditor;
Actions=new-window;new-document;
# TRANSLATORS: Do NOT translate or localize the semicolons!
#              The list MUST also end with a semicolon!
#              Search terms to find this application.
Keywords=Text;Editor;Plaintext;Write;gedit;
DBusActivatable=true
X-Ubuntu-Gettext-Domain=gedit

[Desktop Action new-window]
Name=New Window
Exec=gedit --new-window

[Desktop Action new-document]
Name=New Document
Exec=gedit --new-document
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/org.gnome.Todo.desktop
[Desktop Entry]
Name=To Do
Comment=Manage your personal tasks
Exec=gnome-todo
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=org.gnome.Todo
Terminal=false
Type=Application
NoDisplay=true
StartupNotify=true
Categories=GNOME;GTK;Utility;
# Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
Keywords=Task;Productivity;Todo;
DBusActivatable=true
X-Ubuntu-Gettext-Domain=gnome-todo
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/gnome-session-properties.desktop
[Desktop Entry]
Name=Startup Applications
Comment=Choose what applications to start when you log in
Exec=gnome-session-properties
Icon=session-properties
Terminal=false
Type=Application
NoDisplay=true
StartupNotify=true
Categories=GTK;GNOME;Settings;X-GNOME-PersonalSettings;
X-GNOME-Bugzilla-Bugzilla=GNOME
X-GNOME-Bugzilla-Product=gnome-session
X-GNOME-Bugzilla-Component=gnome-session-properties
X-GNOME-Bugzilla-Version=
X-Ubuntu-Gettext-Domain=gnome-session-3.0
EOF
cat <<'EOF' >/etc/skel/.local/share/applications/org.gnome.Totem.desktop
[Desktop Entry]
Name=Videos
Comment=Play movies
# Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon! Do NOT translate or remove the application name from the list! It is used for search.
Keywords=Video;Movie;Film;Clip;Series;Player;DVD;TV;Disc;Totem;
Exec=totem %U
# Translators: Do NOT translate or transliterate this text (this is an icon file name)!
Icon=org.gnome.Totem
DBusActivatable=true
Terminal=false
Type=Application
NoDisplay=true
Categories=GTK;GNOME;AudioVideo;Player;Video;
X-GNOME-DocPath=totem/totem.xml
X-GNOME-Bugzilla-Bugzilla=GNOME
X-GNOME-Bugzilla-Product=totem
X-GNOME-Bugzilla-Component=general
X-GNOME-Bugzilla-Version=3.38.0
X-GNOME-Bugzilla-OtherBinaries=totem-video-thumbnailer;
StartupNotify=true
MimeType=application/mxf;application/ogg;application/ram;application/sdp;application/vnd.apple.mpegurl;application/vnd.ms-asf;application/vnd.ms-wpl;application/vnd.rn-realmedia;application/vnd.rn-realmedia-vbr;application/x-extension-m4a;application/x-extension-mp4;application/x-flash-video;application/x-matroska;application/x-netshow-channel;application/x-ogg;application/x-quicktimeplayer;application/x-shorten;image/vnd.rn-realpix;image/x-pict;misc/ultravox;text/x-google-video-pointer;video/3gp;video/3gpp;video/3gpp2;video/dv;video/divx;video/fli;video/flv;video/mp2t;video/mp4;video/mp4v-es;video/mpeg;video/mpeg-system;video/msvideo;video/ogg;video/quicktime;video/vivo;video/vnd.divx;video/vnd.mpegurl;video/vnd.rn-realvideo;video/vnd.vivo;video/webm;video/x-anim;video/x-avi;video/x-flc;video/x-fli;video/x-flic;video/x-flv;video/x-m4v;video/x-matroska;video/x-mjpeg;video/x-mpeg;video/x-mpeg2;video/x-ms-asf;video/x-ms-asf-plugin;video/x-ms-asx;video/x-msvideo;video/x-ms-wm;video/x-ms-wmv;video/x-ms-wmx;video/x-ms-wvx;video/x-nsv;video/x-ogm+ogg;video/x-theora;video/x-theora+ogg;video/x-totem-stream;audio/x-pn-realaudio;application/smil;application/smil+xml;application/x-quicktime-media-link;application/x-smil;text/google-video-pointer;x-content/video-dvd;x-scheme-handler/pnm;x-scheme-handler/mms;x-scheme-handler/net;x-scheme-handler/rtp;x-scheme-handler/rtmp;x-scheme-handler/rtsp;x-scheme-handler/mmsh;x-scheme-handler/uvox;x-scheme-handler/icy;x-scheme-handler/icyx;
X-Ubuntu-Gettext-Domain=totem

Actions=play;next-chapter;previous-chapter;mute;fullscreen;

[Desktop Action play]
Name=Play/Pause
Exec=totem --play-pause

[Desktop Action next-chapter]
Name=Next
Exec=totem --next

[Desktop Action previous-chapter]
Name=Previous
Exec=totem --previous

[Desktop Action mute]
Name=Mute
Exec=totem --mute

[Desktop Action fullscreen]
Name=Fullscreen
Exec=totem --fullscreen
EOF
cp -r /etc/skel/.local ~/.local

# Install WayDroid
export DISTRO="hirsute" && \
curl https://repo.waydro.id/waydroid.gpg > /usr/share/keyrings/waydroid.gpg && \
echo "deb [signed-by=/usr/share/keyrings/waydroid.gpg] https://repo.waydro.id/ $DISTRO main" > /etc/apt/sources.list.d/waydroid.list && \
apt-get update && apt-get install -y waydroid lxc mutter
mkdir -p /etc/skel/Desktop
echo "You'll need to run this command to install WayDroid (which I'll be automating in the final build):" > /etc/skel/Desktop/README.txt
echo "sudo waydroid init -i /usr/share/waydroid-extra/images && sudo systemctl start waydroid-container" >> /etc/skel/Desktop/README.txt
echo ""
echo "You'll then need to open the WayDroid app and wait for the window to open. Once open, run this command: sudo waydroid prop set persist.waydroid.multi_windows true && waydroid prop set persist.waydroid.multi_windows true && sudo systemctl restart waydroid-container" >> /etc/skel/Desktop/README.txt
echo "" >> /etc/skel/Desktop/README.txt
echo "You can then open any /e/ app from the launcher. After this, apps should open extremely quickly." >> /etc/skel/Desktop/README.txt
cat > /etc/xdg/autostart/instructions-waydroid.desktop <<'EOF'
[Desktop Entry]
Type=Application
Name=WayDroid Instructions
Comment=Show WayDroid instructions
Exec=gedit /etc/skel/Desktop/README.txt
EOF
mkdir -p /usr/share/waydroid-extra/images && cd /usr/share/waydroid-extra/images
wget https://waydroid.ubuntu-web.org/images/system.img
wget https://waydroid.ubuntu-web.org/images/vendor.img
cd /build

# Remove GNOME Initial Setup's files if they are not already gone
rm -f /usr/libexec/gnome-initial-setup /usr/share/applications/gnome-initial-setup.desktop
