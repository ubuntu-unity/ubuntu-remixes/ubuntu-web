#!/bin/bash

# Set up the environment
dbus-uuidgen > /var/lib/dbus/machine-id
dpkg-divert --local --rename --add /sbin/initctl
ln -s /bin/true /sbin/initctl
mount -t proc none /proc
mount -t sysfs none /sys
mount -t devpts none /dev/pts
export HOME=/root
export LC_ALL=C
KERNEL=$(ls -Art /lib/modules | tail -n 1)
mv "/boot/initrd.fromiso" "/boot/initrd.img-$KERNEL"
mv "/boot/vmlinuz.fromiso" "/boot/vmlinuz-$KERNEL"
cd /build/

# Remve GNOME stuff
for pkg in aisleriot remmina rhythmbox usb-creator-gtk \
 transmission-common gnome-characters yelp gnome-initial-setup \
 thunderbird libreoffice-common gnome-font-viewer \
 apport whoopsie evince baobab gnome-mines gnome-shell-extension-prefs \
 gnome-sudoku seahorse libgnome-games-support-common deja-dup \
 gnome-disk-utility gnome-mahjongg gnome-logs gnome-calendar \
 cheese shotwell gnome-software gnome-contacts ubuntu-session \
 software-properties-gtk snapd;
do apt-get purge -y $pkg; done

# Run the remix script
bash remix.sh && apt-get autoremove -y --purge

# Clean up
update-initramfs -k all -u
apt-get clean
rm -f /var/lib/apt/lists/*_Packages
rm -f /var/lib/apt/lists/*_Sources
rm -f /var/lib/apt/lists/*_Translation-*
rm -rf /tmp/* ~/.bash_history
echo -n > /etc/machine-id
rm -f /var/lib/dbus/machine-id
rm /sbin/initctl
dpkg-divert --rename --remove /sbin/initctl
umount /proc || umount -lf /proc
umount /sys
umount /dev/pts
